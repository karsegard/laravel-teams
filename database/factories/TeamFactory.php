<?php

namespace KDA\Laravel\Teams\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Teams\Models\Team;

class TeamFactory extends Factory
{
    protected $model = Team::class;

    public function definition()
    {
        return [
            //
            'name' => $this->faker->name(),
        ];
    }
}
