<?php

namespace KDA\Laravel\Teams\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamOwnership;

class TeamOwnershipFactory extends Factory
{
    protected $model = TeamOwnership::class;

    public function definition()
    {
        return [
            //
            'team_id' => Team::factory(),
        ];
    }
}
