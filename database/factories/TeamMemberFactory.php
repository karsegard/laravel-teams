<?php

namespace KDA\Laravel\Teams\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamMember;

class TeamMemberFactory extends Factory
{
    protected $model = TeamMember::class;

    public function definition()
    {
        return [
            'team' => Team::factory(),
        ];
    }
}
