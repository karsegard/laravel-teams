<?php

namespace KDA\Laravel\Teams\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamInvitation;

class TeamInvitationFactory extends Factory
{
    protected $model = TeamInvitation::class;

    public function definition()
    {
        return [
            'team' => Team::factory(),
            'email' => $this->faker->email(),

        ];
    }
}
