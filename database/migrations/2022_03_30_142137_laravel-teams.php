<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->unique(['name']);
        });

        Schema::create('team_member', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id');
            $table->numericMorphs('member');
            $table->boolean('owner')->default(0);
            $table->timestamps();
            $table->unique(['team_id', 'member_id', 'member_type']);
        });

        Schema::create('team_ownerships', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id');
            $table->numericMorphs('owned');
            $table->timestamps();
            $table->unique(['owned_id', 'owned_type']);
        });

        Schema::create('team_invitations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id')->constrained()->cascadeOnDelete();
            $table->string('email');
            //  $table->string('role')->nullable();
            $table->timestamps();
            $table->unique(['team_id', 'email']);
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('teams');
        Schema::dropIfExists('team_member');
        Schema::dropIfExists('team_ownerships');
        Schema::dropIfExists('team_invitations');
        Schema::enableForeignKeyConstraints();
    }
};
