<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Teams\Models\Traits\OwnedByTeam;

class Post extends Model
{
    use HasFactory;
    use OwnedByTeam;

    protected $fillable = [
        'title',
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
