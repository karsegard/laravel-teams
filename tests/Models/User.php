<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Teams\Models\Traits\HasTeams;

class User extends Model
{
    use HasTeams;
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\UserFactory::new();
    }
}
