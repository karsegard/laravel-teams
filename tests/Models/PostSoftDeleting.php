<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use KDA\Laravel\Teams\Models\Traits\OwnedByTeam;

class PostSoftDeleting extends Model
{
    use HasFactory;
    use OwnedByTeam;
    use SoftDeletes;

    protected $table = 'posts';

    protected $fillable = [
        'title',
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
