<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostWithoutTrait extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
        'title',
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
