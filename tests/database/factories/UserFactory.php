<?php

namespace KDA\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\User;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        return [
            'name' => $this->faker->userName(),
        ];
    }
}
