<?php

namespace KDA\Tests;

use Illuminate\Foundation\Testing\RefreshDatabaseState;
use KDA\Laravel\Teams\ServiceProvider as ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;

class TestCase extends ParentTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [

            ServiceProvider::class,
        ];
    }

    protected function runPackageMigrations($app)
    {
        $provider = $this->getPackageProviders($app)[0];
        $traits = class_uses_recursive($provider);

        if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
            $this->artisan('migrate', ['--database' => 'mysql'])->run();
            $this->beforeApplicationDestroyed(function () {
                $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
                RefreshDatabaseState::$migrated = false;
            });
        }
    }

    protected function callAfterResolving($name, $callback)
    {
        $this->app->afterResolving($name, $callback);

        if ($this->app->resolved($name)) {
            $callback($this->app->make($name), $this->app);
        }
    }

    protected function loadTestMigrations($paths)
    {
        $this->callAfterResolving('migrator', function ($migrator) use ($paths) {
            foreach ((array) $paths as $path) {
                $migrator->path($path);
            }

            $this->beforeApplicationDestroyed(function () {
                $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
                RefreshDatabaseState::$migrated = false;
            });
        });
    }

    protected function getEnvironmentSetUp($app)
    {
        $this->app = $app;
        $this->loadTestMigrations([__DIR__.'/../database/migrations', __DIR__.'/database/migrations']);
        $this->runPackageMigrations($app);
    }
}
