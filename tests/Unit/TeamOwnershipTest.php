<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Teams\Facades\Team as FacadesTeam;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamOwnership;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostSoftDeleting;
use KDA\Tests\Models\PostWithoutTrait;
use KDA\Tests\TestCase;

class TeamOwnershipTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function ownership_relations()
    {
        $o = Team::factory()->create(['name' => 'my team']);

        $post = PostWithoutTrait::create(['title' => 'hello']);
        $ownership = TeamOwnership::factory()->create(['owned_id' => $post->id, 'owned_type' => get_class($post)]);
        $this->assertNotNull($ownership->resource);
        $this->assertNotNull($ownership->team);
    }

 /** @test */
 public function no_relationship_created_when_no_team()
 {
     $o = Team::factory()->create(['name' => 'my team']);

     // FacadesTeam::setCurrentTeam($o->id);

     $post = Post::create(['title' => 'hello']);
     $o = TeamOwnership::all();
     $this->assertEquals(0, $o->count());
 }

 /** @test */
 public function automatic_relationcreation()
 {
     $team = Team::factory()->create(['name' => 'my team']);

     FacadesTeam::setCurrentTeam($team->id);

     $post = Post::create(['title' => 'hello']);
     $o = TeamOwnership::all();
     $this->assertEquals(1, $o->count());
 }

 /** @test */
 public function ownership_is_forgotten()
 {
     $team = Team::factory()->create(['name' => 'my team']);
     FacadesTeam::setCurrentTeam($team->id);
     $post = Post::create(['title' => 'hello']);
     $o = TeamOwnership::all();
     $this->assertEquals(1, $o->count());

     $post->delete();
     $o = TeamOwnership::all();
     $this->assertEquals(0, $o->count());
 }

 /** @test */
 public function ownership_is_not_forgotten_when_soft_deletes()
 {
     $team = Team::factory()->create(['name' => 'my team']);
     FacadesTeam::setCurrentTeam($team->id);
     $post = PostSoftDeleting::create(['title' => 'hello']);
     $o = TeamOwnership::all();
     $this->assertEquals(1, $o->count());

     $post->delete();
     $o = TeamOwnership::all();
     $this->assertEquals(1, $o->count());
 }

 /** @test */
 public function ownership_is_forgotten_when_force_deletes()
 {
     $team = Team::factory()->create(['name' => 'my team']);
     FacadesTeam::setCurrentTeam($team->id);
     $post = PostSoftDeleting::create(['title' => 'hello']);
     $o = TeamOwnership::all();
     $this->assertEquals(1, $o->count());

     $post->forceDelete();
     $o = TeamOwnership::all();
     $this->assertEquals(0, $o->count());
 }

 /** @test */
 public function ownership_team_relationship()
 {
     $team = Team::factory()->create();
     FacadesTeam::setCurrentTeam($team->id);
     $post = Post::create(['title' => 'hello']);
     $this->assertNotNull($post->team_ownership);
 }

 /** @test */
 public function model_belongsToTeam()
 {
     $team = Team::factory()->create();
     $team2 = Team::factory()->create();
     FacadesTeam::setCurrentTeam($team->id);
     $post = Post::create(['title' => 'hello']);
     $this->assertTrue($post->belongsToTeam($team));
     $this->assertFalse($post->belongsToTeam($team2));
 }

 /** @test */
 public function unowned_model_belongsToTeam_always_false()
 {
     $team = Team::factory()->create();
     $team2 = Team::factory()->create();
     $post = Post::create(['title' => 'hello']);
     $this->assertFalse($post->belongsToTeam($team));
     $this->assertFalse($post->belongsToTeam($team2));
 }

 /** @test */
 public function unowned_model_belongsToNoTeam()
 {
     $team = Team::factory()->create();
     $team2 = Team::factory()->create();
     $post = Post::create(['title' => 'hello']);
     $this->assertTrue($post->belongsToNoTeam());
 }
}
