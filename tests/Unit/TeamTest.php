<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Teams\Models\Team;
use KDA\Tests\TestCase;

class TeamTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_team()
    {
        $o = Team::factory()->create(['name' => 'my team']);
        $this->assertNotNull($o);
    }
}
