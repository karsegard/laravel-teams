<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Teams\Facades\Team as FacadesTeam;
use KDA\Laravel\Teams\Models\Team;
use KDA\Tests\TestCase;

class ManagerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function set_current_team_default()
    {
        $team = FacadesTeam::getCurrentTeam();
        $this->assertNull($team);
        $t = Team::factory()->create(['name' => 'my team']);
        FacadesTeam::setCurrentTeam($t->id);
        $this->assertEquals(FacadesTeam::getCurrentTeam()->id, $t->id);
    }

    /** @test */
    public function set_wrong_team_object_or_id_throws()
    {
        $this->expectException(\Exception::class);
        FacadesTeam::setCurrentTeam(14);
    }

    /** @test */
    public function set_team_model_works()
    {
        $t = Team::factory()->create(['name' => 'my team']);
        FacadesTeam::getCurrentTeamUsing(function () use ($t) {
            return $t;
        });

        $this->assertEquals(FacadesTeam::getCurrentTeam()->id, $t->id);
    }
}
