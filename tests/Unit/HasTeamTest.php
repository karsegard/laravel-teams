<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Teams\Models\Team;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;

class HasTeamTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_test_user()
    {
        $o = User::factory()->create();
        $this->assertNotNull($o);
        $this->assertEquals(0, $o->memberships->count());
    }

    /** @test */
    public function can_join_team()
    {
        $o = User::factory()->create();
        $t = Team::factory()->create();

        $o->joinTeam($t);
        $this->assertEquals(true, $o->isMemberOf($t));
        $this->assertEquals(1, $o->memberships->count());
    }

    /** @test */
    public function cannot_join_team_twice()
    {
        $this->expectException(\Exception::class);
        $o = User::factory()->create();
        $t = Team::factory()->create();

        $o->joinTeam($t);
        $o->joinTeam($t);
        $this->assertEquals(1, $o->memberships->count());
    }

    /** @test */
    public function can_join_as_owner()
    {
        $o = User::factory()->create();
        $o2 = User::factory()->create();
        $t = Team::factory()->create();

        $o->joinTeam($t, true);
        $o2->joinTeam($t);

        $this->assertEquals(1, $o->memberships->count());
        $this->assertEquals(1, $o2->memberships->count());
    }

      /** @test */
      public function can_leave_team()
      {
          $o = User::factory()->create();
          $o2 = User::factory()->create();
          $t = Team::factory()->create();

          $o->joinTeam($t, true);
          $o2->joinTeam($t);
          $this->assertEquals(1, $o->memberships->count());
          $this->assertEquals(1, $o2->memberships->count());

          $o->leaveTeam($t);
          $this->assertEquals(0, $o->fresh()->memberships->count());
          $this->assertEquals(1, $o2->memberships->count());
      }

      /** @test */
      public function can_check_membership()
      {
          $user1 = User::factory()->create();
          $user2 = User::factory()->create();
          $user3 = User::factory()->create();
          $team1 = Team::factory()->create();
          $team2 = Team::factory()->create();
          $team3 = Team::factory()->create();

          $user1->joinTeam($team1);
          $user2->joinTeam($team2);
          $user3->joinTeam($team3, true);
          $user2->joinTeam($team3);

          $this->assertTrue($user1->isMemberOf($team1));
          $this->assertTrue($user2->isMemberOf($team2));

          $this->assertFalse($user1->isMemberOf($team3));
          $this->assertFalse($user1->isMemberOf($team2));

          $this->assertFalse($user2->isOwnerOf($team3));
          $this->assertFalse($user2->isMemberOf($team1));
          $this->assertTrue($user2->isMemberOf($team3));

          $this->assertTrue($user3->isOwnerOf($team3));
          $this->assertTrue($user3->isMemberOf($team3));
      }
}
