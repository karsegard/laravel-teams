<?php

namespace KDA\Laravel\Teams\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\Laravel\Teams\Facades\Team;

class OwnershipCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:laravel-teams:give:ownership {model} {team} {--A|all} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $model = $this->argument('model');
        $team = $this->argument('team');
        $all = $this->options('all');

        $model = resolve($model);
        $class = $model::class;
        $team = Team::retrieveTeam($team);

        $class::when($all==false, function ($q) {
            return $q->whereDoesntHave('team_ownership');
        })->each(function ($m) use($team){
            Team::registerOwnership($m,$team);
        });
    }
}
