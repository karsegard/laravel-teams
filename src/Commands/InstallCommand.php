<?php

namespace KDA\Laravel\Teams\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:laravel-teams:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install laravel-teams files';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', ['--provider' => "KDA\Laravel\Teams\ServiceProvider", '--tag' => 'migrations']);
    }
}
