<?php

namespace KDA\Laravel\Teams;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Teams\Commands\InstallCommand;
use KDA\Laravel\Teams\Commands\OwnershipCommand;
use KDA\Laravel\Teams\Facades\Team;
use KDA\Laravel\Traits\HasDumps;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasLoadableMigration;
  //  use \KDA\Laravel\Traits\HasMigration;
    use HasDumps;

    protected $dumps =[
        'teams',
        'team_member',
        'team_ownerships',
        'team_invitations',
    ];

    protected $_commands = [
        InstallCommand::class,
        OwnershipCommand::class,
    ];

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

     // trait \KDA\Laravel\Traits\HasConfig;
     //    registers config file as
     //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/laravel-teams.php' => 'kda.laravel-teams',
    ];

    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();

        $this->app->singleton(Team::class, function () {
            return new TeamManager();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
