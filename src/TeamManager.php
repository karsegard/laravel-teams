<?php

namespace KDA\Laravel\Teams;

use Closure;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamMember;
use KDA\Laravel\Teams\Models\TeamOwnership;
use Illuminate\Database\Eloquent\Builder;

class TeamManager
{
    protected Closure|null $getCurrentTeamUsing;
    protected Closure|null $getDefaultTeamUsing = null;
    protected Closure|null $setCurrentTeamUsing;
    protected Closure|bool $allow_no_team = false;

    public function noTeamAllowed(Closure|bool $allow_no_team): static
    {
        $this->allow_no_team = $allow_no_team;
        return $this;
    }

    public function isNoTeamAllowed()
    {
        return $this->evaluate($this->allow_no_team);
    }

    public function getCurrentTeamUsing($closure)
    {
        $this->getCurrentTeamUsing = $closure;
    }

    public function getDefaultTeamUsing($closure)
    {
        $this->getDefaultTeamUsing = $closure;
    }

    public function setCurrentTeamUsing($closure)
    {
        $this->setCurrentTeamUsing = $closure;
    }

    public function getCurrentTeam()
    {
        $team = $this->evaluate($this->getCurrentTeamUsing);

        if (blank($team) && !$this->isNoTeamAllowed()) {
            $team = $this->getDefaultTeam();
        }

        if (!blank($team) && !$team instanceof Team) {
            $team = Team::find($team);
        }

        return $team;
    }

    public function setCurrentTeam(null|Team|string|int $team = null)
    {
        if (!blank($team) && !$team instanceof Team) {
            $team = Team::find($team);
        }
        if (!$team && !$this->isNoTeamAllowed()) {
            throw new \Exception('Wrong team or not existent');
        }

        return $this->evaluate($this->setCurrentTeamUsing, ['team' => $team?->getKey()]);
    }

    public function getDefaultTeam()
    {
        $team = $this->evaluate($this->getDefaultTeamUsing);
        if (!$team instanceof Team) {
            $team = Team::find($team);
        }
        return $team;
    }

    public function evaluate($value, array $parameters = [])
    {
        if ($value instanceof Closure) {
            return app()->call($value, $parameters);
        }

        return $value;
    }

    public function __construct()
    {
        $this->getCurrentTeamUsing(function () {
            return session()->get('team');
        });

        $this->setCurrentTeamUsing(function ($team) {
            return session()->put('team', $team);
        });

        $this->getDefaultTeamUsing(function () {
            return $this->getTeams()?->first();
        });
    }

    public function registerOwnership($model, ?Team $currentTeam = null)
    {
        $currentTeam ??= $this->getCurrentTeam();

        $currentTeam = $currentTeam?->id;
        if (!blank($currentTeam) && blank($model->fresh()->team_ownership)) {
            $key = $model->getKey();
            TeamOwnership::create([
                'team_id' => $currentTeam,
                'owned_id' => $key,
                'owned_type' => get_class($model),
            ]);
        }
    }

    public function forgetOwnership($model,$force=false)
    {
        $soft_deleting = in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model)) && !$model->isForceDeleting();

        if (!$soft_deleting || $force) {
            $currentTeam = $this->getCurrentTeam()?->id;

            if (!blank($currentTeam)) {
                $key = $model->getKey();
                TeamOwnership::where([
                    'team_id' => $currentTeam,
                    'owned_id' => $key,
                    'owned_type' => get_class($model),
                ])->delete();
            }
        }
    }

    public function moveOwnership($model,$newTeam){
        TeamOwnership::where([
            'owned_id' => $model->getKey(),
            'owned_type' => get_class($model),
        ])->delete();
        $this->registerOwnership($model,$newTeam);
    }

    public function joinTeam($model, $team, $owner = false)
    {
        $model->memberships()->attach($team, ['owner' => $owner]);
    }

    public function leaveTeam($model, $team)
    {
        $model->memberships()->detach($team);
    }

    protected function evaluateTeam(Team|string|int|Closure $team): Team
    {
        if ($team instanceof Team) {
            $team = $team->fresh();
        } elseif (is_numeric($team)) {
            $team = Team::find($team);
        } elseif (is_string($team)) {
            $team = Team::where('name', $team)->first();
        } else {
            $team = $this->evaluate($team);
        }

        return $team;
    }

    protected function evaluateModel(Model|Closure $model)
    {
        if ($model instanceof Model) {
            $model = $model->fresh();
        } else {
            $model = $this->evaluate($model);
        }

        return $model;
    }

    public function modelIsMemberOf(Model|Closure $model, Team|string|int|Closure $team, $owner = false): bool
    {
        $team = $this->evaluateTeam($team);
        $model = $this->evaluatemodel($model);
        $q = Team::whereHas('members', function ($query) use ($model, $owner) {
            return $query->whereHasMorph('related', get_class($model), function ($query) use ($model, $owner) {
                $query->where('member_id', $model->getKey());
                if ($owner === true) {
                    $query->where('owner', true);
                }
            });
        })->where('id', $team->getKey());

        return $q->count() > 0;
    }

    public function getTeams(Model|null $user = null)
    {
        if (blank($user)) {
            $user = auth()->user();
        }

        return $user?->teams;
    }

    public function retrieveTeam($team): Team
    {
        return $this->evaluateTeam($team);
    }

    public function registerOwnableModel($model, $withGlobalScope = false, $withHooks = true)
    {
        $model::resolveRelationUsing('team_ownership', function ($orderModel) {
            return $orderModel->morphOne(TeamOwnership::class, 'owned');
        });

        if ($withGlobalScope) {
            $model::addGlobalScope('forCurrentTeam', function (Builder $q) {
                return $q->when(!blank($this->getCurrentTeam()), function ($q) {
                    $q->whereHas('team_ownership', function ($q) {
                        $q->where('team_id', $this->getCurrentTeam()->getKey());
                    });
                });
            });
        }
        if ($withHooks) {
            $model::updated(function ($model) {
                $this->registerOwnership($model);
            });
            $model::created(function ($model) {
                $this->registerOwnership($model);
            });
            $model::deleting(function ($model) {
                $this->forgetOwnership($model);
            });
        }
    }
}
