<?php

namespace KDA\Laravel\Teams\Models\Traits;

use KDA\Laravel\Teams\Facades\Team as FacadesTeam;
use KDA\Laravel\Teams\Models\Relations\Member;
use KDA\Laravel\Teams\Models\Team;
use KDA\Laravel\Teams\Models\TeamMember;

trait HasTeams
{
    public static function bootHasTeams(): void
    {
        static::created(
            function ($model) {
            }
        );
    }

   /* public function memberships(){
        return $this->morphMany(TeamMember::class,'member');
    }*/

    public function memberships()
    {
        return $this->morphToMany(Team::class, 'member', 'team_member', 'member_id', 'team_id')
                    ->using(Member::class)->as('memberships')->withPivot('id', 'owner');
    }

    public function joinTeam($team, $owner = false)
    {
        FacadesTeam::joinTeam($this, $team, $owner);
    }

    public function leaveTeam($team)
    {
        FacadesTeam::leaveTeam($this, $team);
    }

    public function isMemberOf(Team | string | int | Closure $team)
    {
        return FacadesTeam::modelIsMemberOf($this, $team);
    }

    public function isOwnerOf(Team | string | int | Closure $team)
    {
        return FacadesTeam::modelIsMemberOf($this, $team, true);
    }

    public function teams()
    {
        return $this->morphToMany(Team::class, 'member','team_member');
    }
/*

    public function has_many_polymorphic()
    {
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
