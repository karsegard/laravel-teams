<?php

namespace KDA\Laravel\Teams\Models\Traits;

use KDA\Laravel\Teams\Facades\Team;
use KDA\Laravel\Teams\Models\Team as ModelsTeam;
use KDA\Laravel\Teams\Models\TeamOwnership;

trait OwnedByTeam
{
    public static function bootOwnedByTeam(): void
    {
        static::created(
            function ($model) {
                Team::registerOwnership($model);
            }
        );
        static::updating(
            function ($model) {
            }
        );
        static::deleting(
            function ($model) {
                Team::forgetOwnership($model);
            }
        );
    }

    public function belongsToTeam(ModelsTeam $team)
    {
        /* $team_id = $this->team_ownership?->team?->getKey();*/
        return $this->team?->getKey() == $team->getKey();
    }

    public function belongsToNoTeam()
    {
        /* $team_id = $this->team_ownership?->team?->getKey();*/
        return $this->team == null;
    }

    public function getTeamAttribute()
    {
        return $this->team_ownership?->team;
    }

    public function team_ownership()
    {
        return $this->morphOne(TeamOwnership::class, 'owned');
    }

    public function scopeForCurrentTeam($q){
        return $q->whereHas('team_ownership',function($q){
            $q->where('team_id',Team::getCurrentTeam()->getKey());
        });
    }
    public function scopeForAnyTeam($q){
        return $q->whereHas('team_ownership',function($q){
            $q->whereIn('team_id',Team::getTeams()->pluck('id','id'));
        });
    }
}
