<?php

namespace  KDA\Laravel\Teams\Models\Relations;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use  KDA\Laravel\Teams\Models\Team;

class Member extends MorphPivot
{
    protected $table = 'team_member';

    protected $guarded = [];

    public $incrementing = true;

    public $casts = [
        'owner' => 'boolean',
    ];

    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    /*public function media()
    {
        return $this->belongsTo(MediaLibraryItem::class,'medialibrary_item_id');
    }*/
    public function related()
    {
        return $this->morphTo(__FUNCTION__, 'member_type', 'member_id');
    }
}
