<?php

namespace KDA\Laravel\Teams\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamInvitation extends Model
{
    use HasFactory;

    protected $fillable = [

    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  \KDA\Laravel\Teams\Database\Factories\TeamInvitationFactory::new();
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
