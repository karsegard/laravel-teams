<?php

namespace KDA\Laravel\Teams\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamOwnership extends Model
{
    use HasFactory;

    protected $fillable = [
        'team_id',
        'owned_id',
        'owned_type',
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function owned()
    {
        return $this->morphTo();
    }

    protected static function newFactory()
    {
        return  \KDA\Laravel\Teams\Database\Factories\TeamOwnershipFactory::new();
    }
}
