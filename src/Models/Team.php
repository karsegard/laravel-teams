<?php

namespace KDA\Laravel\Teams\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  KDA\Laravel\Teams\Models\Relations\Member;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  \KDA\Laravel\Teams\Database\Factories\TeamFactory::new();
    }

    public function members()
    {
        return $this->hasMany(Member::class, 'team_id');
    }

    public function users()
    {
        return $this->morphedByMany(\App\Models\User::class, 'member','team_member');
    }

    public function getMembersByTypeAttribute()
    {
        return $this->members()->get()->groupBy('member_type');
    }
}
